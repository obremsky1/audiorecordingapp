//
//  AudioInput.swift
//  AudioRecordingApp
//
//  Created by Viacheslav Obremsky on 10/3/17.
//  Copyright © 2017 Viacheslav Obremsky. All rights reserved.
//

import UIKit
import AVFoundation

class AudioInput: NSObject, Input, AVAudioRecorderDelegate {
    init(string : String) {
    }
    
    var completionHandler: ((String?, Error?) -> Void)?

    var recordingSession: AVAudioSession!
    var recorder: AVAudioRecorder!
    var url : URL?
  let recordSettings = [
        AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
        AVSampleRateKey: 44100,
        AVNumberOfChannelsKey: 2,
        AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
    ]
    
    func open(completion: @escaping (Error?) -> Void) -> Void {
       
       let path = "\(PathBuilder.defaultCachePath())/\(FilenameBuilder.createTempName())";
       
       url  = URL.init(fileURLWithPath: path)
        recordingSession = AVAudioSession.sharedInstance()
        
        if let url = self.url {
            do {
               
                try FileManager.default.createDirectory(at: URL.init(fileURLWithPath: PathBuilder.defaultCachePath()), withIntermediateDirectories: true, attributes: nil)
                recordingSession = AVAudioSession.sharedInstance()
                try! recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord, with: AVAudioSessionCategoryOptions.defaultToSpeaker)
              try! recordingSession.setActive(true)
                recorder = try AVAudioRecorder(url: url, settings: recordSettings)
                recorder.delegate = self
                recorder.record()
                
            } catch {
                print ("Failed to init the recorder")
            }
        }
    }
    
    func close(completion: @escaping (String?, Error?) -> Void) {
        self.completionHandler = completion
        if let recorder = self.recorder{
            recorder.stop()
        }
    }
    
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        
        if let completion = self.completionHandler {
           completion(self.url?.path, nil)
        }
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
         if let completion = self.completionHandler {
           completion(nil, error)
        }
    }
}
