//
//  Input.swift
//  AudioRecordingApp
//
//  Created by Viacheslav Obremsky on 10/3/17.
//  Copyright © 2017 Viacheslav Obremsky. All rights reserved.
//

import Foundation

protocol Input {
    func open(completion: @escaping (Error?)->Void)
    func close(completion: @escaping (String?, Error?)->Void)
}
