//
//  FileOutput.swift
//  AudioRecordingApp
//
//  Created by Viacheslav Obremsky on 10/3/17.
//  Copyright © 2017 Viacheslav Obremsky. All rights reserved.
//

import UIKit

class FileOutput: NSObject, Output {
    
    var destination : String
    var cachedFile : String?

    
    init(destination : String) {
      self.destination = destination;
    }
    func processWithTempolaryFile(path : String, completion: @escaping (Error?)->Void){
        
        var handledError : Error?
        
        cachedFile = path;
        let fileName = FilenameBuilder.createName()
        let newPath = "\(destination)/\(fileName)";
        
         var isDir : ObjCBool = true
        
        let oldUrl = URL.init(fileURLWithPath: path)
        let newUrl = URL.init(fileURLWithPath: newPath)
        
        do {
            try FileManager.default.createDirectory(at: URL.init(fileURLWithPath: destination), withIntermediateDirectories: true, attributes: nil)
            try FileManager.default.copyItem(at: oldUrl, to: newUrl)
            
        } catch {
            print(error.localizedDescription)
            handledError = error
        }
        completion(handledError);
    }
}
