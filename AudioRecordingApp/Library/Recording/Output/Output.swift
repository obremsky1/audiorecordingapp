//
//  Output.swift
//  AudioRecordingApp
//
//  Created by Viacheslav Obremsky on 10/3/17.
//  Copyright © 2017 Viacheslav Obremsky. All rights reserved.
//

import Foundation

protocol Output {
    func processWithTempolaryFile(path : String, completion: @escaping (Error?)->Void)
}
