//
//  Recorder.swift
//  AudioRecordingApp
//
//  Created by Viacheslav Obremsky on 10/3/17.
//  Copyright © 2017 Viacheslav Obremsky. All rights reserved.
//

import UIKit

class Recorder: NSObject {
    
    var input : Input
    var output : Output
    
    init(input : Input, output : Output) {
        self.input = input
        self.output = output
    }
    
    func start() -> Void {
        input.open { (error) in
            
        }
    }
    
    func stop(completion: @escaping (Error?)->Void) -> Void {
        
        var savingError : Error?
        input.close (completion : { (tempUrl, error) in
            savingError = error
            if let path = tempUrl{
                print(path)
                self.output.processWithTempolaryFile(path: path, completion: {(error) in
                    
                    savingError = error
                    do {
                        try FileManager.default.removeItem(at: URL.init(fileURLWithPath: path))
                    }catch{
                        savingError = error
                    }
                    
                    completion(savingError)
                })
            } else {
                completion(savingError)
            }
        })
    }
}
