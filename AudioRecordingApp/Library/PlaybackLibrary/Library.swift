//
//  MusicLibrary.swift
//  AudioRecordingApp
//
//  Created by Viacheslav Obremsky on 10/3/17.
//  Copyright © 2017 Viacheslav Obremsky. All rights reserved.
//

import UIKit

class Library: NSObject {

     var player : Player
     var storage : Storage

     init(player : Player, storage : Storage) {
        self.player = player;
        self.storage = storage;
    }
    
    func play() -> Void {
      refresh()
      player.play()
    }
    
    func stop() -> Void {
        player.stop()
    }
    
    func refresh ()-> Void {
      var assets = storage.fetch()
        player.assets = assets;
    }
}
