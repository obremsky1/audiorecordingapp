//
//  AudioPlayer.swift
//  AudioRecordingApp
//
//  Created by Viacheslav Obremsky on 10/3/17.
//  Copyright © 2017 Viacheslav Obremsky. All rights reserved.
//

import UIKit
import AVFoundation

class AudioPlayer: Player, AVAudioPlayerDelegate {
    
    var audioPlayer : AVAudioPlayer?
    
    override func play() -> Void {
        if let assets = self.assets{
            let first = assets.first
            if let firstSong = first {
                do {
                    let url = URL.init(fileURLWithPath: firstSong)
                    print(url)
                    try audioPlayer = AVAudioPlayer(contentsOf: url)
                    if let audioPlayer = audioPlayer{
                        audioPlayer.delegate = self
                        audioPlayer.volume = 1.0
                        audioPlayer.prepareToPlay()
                        audioPlayer.play()
                    }
                }catch {
                    print(error)
                }
            }
        }
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print(error)
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("finished")
        
        if let playbackHandler = playbackHandler {
         playbackHandler();
        }
    }
    
    override func stop(){
        if let player = self.audioPlayer{
            player.stop()
        }
    }
}
