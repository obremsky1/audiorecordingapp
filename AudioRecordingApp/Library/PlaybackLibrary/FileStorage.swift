//
//  FileStorage.swift
//  AudioRecordingApp
//
//  Created by Viacheslav Obremsky on 10/3/17.
//  Copyright © 2017 Viacheslav Obremsky. All rights reserved.
//

import UIKit

class FileStorage: Storage
{
    var folderPath : String
    
    init(sourceFolder : String){
        folderPath = sourceFolder
    }
    
    override func fetch() -> [String]? {
        var array : [String]?
        do{
            array = try FileManager.default.contentsOfDirectory(atPath: folderPath)
        } catch{
            
        }
        if let tempArray = array{
            var fullLinks = [String]()
            for name in tempArray {
                fullLinks.append(folderPath + "/\(name)")
            }
            array = fullLinks.reversed()
        }
        return array;
    }
}
