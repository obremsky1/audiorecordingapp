//
//  PathBuilder.swift
//  AudioRecordingApp
//
//  Created by Viacheslav Obremsky on 10/3/17.
//  Copyright © 2017 Viacheslav Obremsky. All rights reserved.
//

import UIKit

class PathBuilder: NSObject {
    class func defaultAudioFilesPath() -> String
    {
      let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
      let path  = "\(documentsPath)/audio"
        return path
    }
    
     class func defaultCachePath() -> String
    {
      let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
      let path  = "\(documentsPath)/cache"
      return path
    }
}
