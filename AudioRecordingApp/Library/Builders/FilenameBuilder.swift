//
//  FilenameBuilder.swift
//  AudioRecordingApp
//
//  Created by Viacheslav Obremsky on 10/3/17.
//  Copyright © 2017 Viacheslav Obremsky. All rights reserved.
//

import UIKit

class FilenameBuilder {

    class func createName() -> String
    {
        let timestamp = Date();
        let formatter = DateFormatter()
        
        formatter.dateFormat = "ddMMyyyyHHmmSS"
        
        let result = formatter.string(from: timestamp)
        
        let name = "\(result)"  + ".m4a"
        return name;
    }
    
    class func createTempName() -> String
    {
        return "tmp" + self.createName();
    }
}
