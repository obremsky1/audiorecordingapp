//
//  ViewController.swift
//  AudioRecordingApp
//
//  Created by Viacheslav Obremsky on 10/3/17.
//  Copyright © 2017 Viacheslav Obremsky. All rights reserved.
//

import UIKit

enum State {
    case normal
    case recording
    case playing
}

class ViewController: UIViewController {
    
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var stopPlayingButton: UIButton!
    @IBOutlet weak var stopRecordingButton: UIButton!
    
    var  recorder  :Recorder?
    var  library   :Library?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Needs to placed in separated manager
        let storagePath = PathBuilder.defaultAudioFilesPath()
        var isDir : ObjCBool = true
        if (!FileManager.default.fileExists(atPath: storagePath, isDirectory: &isDir)){
            do{
                try FileManager.default.createDirectory(at: URL.init(fileURLWithPath: storagePath), withIntermediateDirectories: true, attributes: nil)
            } catch{
                
            }
        }
        
        let audioInput = AudioInput(string : "")
        let fileOutput = FileOutput(destination : storagePath)
        recorder = Recorder(input : audioInput, output : fileOutput)
        
        let fileStorage = FileStorage(sourceFolder: storagePath);
        let player = AudioPlayer();
        
        player.playbackHandler = {[unowned self]  in
            self.playbackDidFinish()
        };
        library = Library(player : player, storage : fileStorage)
        
        updateForState(state: State.normal)
    }
    
    func playbackDidFinish() -> Void {
        updateForState(state: State.normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func play(_ sender: Any) {
        if let library = self.library{
            library.play()
           updateForState(state: State.playing)

        }
    }
    
    @IBAction func stopPlaying(_ sender: Any) {
        if let library = self.library{
            library.stop()
            updateForState(state: State.normal)
            
        }
    }
    
    @IBAction func stop(_ sender: Any) {
        if let recorder = self.recorder{
            recorder.stop(completion: { (error) in
                if let library = self.library{
                    library.refresh();
                }
                
            })
            updateForState(state: State.normal)
        }
    }
    
    @IBAction func record(_ sender: Any) {
        if let recorder = self.recorder{
            recorder.start()
            updateForState(state: State.recording)
        }
    }
    
    
    func updateForState(state : State) -> Void {
        
        switch state {
        case .normal:
            self.recordButton.isEnabled = true
            self.playButton.isEnabled = true
            self.stopPlayingButton.isEnabled = false
            self.stopRecordingButton.isEnabled = false
        case .recording:
            self.recordButton.isEnabled = false
            self.playButton.isEnabled = false
            self.stopPlayingButton.isEnabled = false
            self.stopRecordingButton.isEnabled = true
        case .playing:
            self.recordButton.isEnabled = false
            self.playButton.isEnabled = false
            self.stopPlayingButton.isEnabled = true
            self.stopRecordingButton.isEnabled = false
        }
    }
}

